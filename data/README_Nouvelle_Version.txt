Le but de ce projet est de pouvoir annoter un nouveau génome ABC à partir de systèmes ABC déjà annotés.

Choix des tables:

Matrice issu de la version précédente du projet:Gene_ID,Function,Chromosome,Strain_ID,Species,Assembly_Name,Domain_Structure,Identification_Status,Domain_Organization,Functional_Classification,Score,e_Value,Description.

Table "Orthologie": Query_ID, Subject_ID, identity,alignment,length 

Principe:
En se servant de la matrice précédante, nous allons réaliser une matrice de comptage comprennant autant de colonne que de sous famille (A1 à A12). Le but est ensuite de servir des données de la table orthologie afin de remplir la matrice de comptage. Le subject_id  de chaque ligne d'orthologie est mis en relation avec le Gene_ID issu de la matrice. Si un même Gene_ID est trouvé, on regarde la sous-famille associée. La matrice de comptage est alors incrémentée de 1 pour la case de la sous-famille qui correspond à ce gène ID.
Cette matrice de comptage comprendra aussi des genes qui sont sûrs de ne pas être ABC (toutes les valeurs des cases sont alors à 0).
Une dernière colonne class sera ajoutée et qui renseignera sur le nom de la sous famille associée à la ligne, dans le cas des non ABC elle sera à OTHER.

Nous générerons aussi une seconde matrice de comptage qui comprend les gènes issu de la partie de la base non expertisée et qui seront ceux que nous cherchons à annoter.

La matrice finale est de la forme:
Gene_ID,A1,A2,A4,A5,A6,A7,A8,A9,A10,A11,A12,class

Pour la partie analyse de notre projet, nous nous servirons de ces matrices afin de réaliser des prédictions sur Knime.

