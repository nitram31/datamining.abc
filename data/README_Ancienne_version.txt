Le but de ce projet est de pouvoir annoter un nouveau génome ABC à partir de systèmes ABC déjà annotés.

Choix des tables:
Plusieurs tables seront sélectionnées:

Assembly:
Afin de savoir quels systèmes sont complets et donc ceux que nous garderons.

Protein:les:
Plusieurs tables seront sélectionnées:
﻿
Assembly:
Afin de savoir quels systèmes sont complets et donc ceux que nous garderons.
﻿
Protein:
A partir des systèmes sélectionnés garder les gènes, chromosomes associés.
﻿
Gene:
Même principe que protéine et permet de faire le liens avec la table Chromosome, Conserved Domain,Functional Domain. On conservera aussi la fonction pour apporter des informations lors de l’analyse.
﻿
Chromosome:
Conserver la lignée.
﻿
Strain:
Garder l’espèce.
﻿
Conserved Domain:
Garder le score et la E value qui servira pour l’analyse.
﻿
Functional Domain:
On conserve la description pour apporter des informations supplémentaires lors de l’analyse.
﻿
Principe:
Dans un premier temps, on récupère toutes les lignes dans Assembly qui contiennent au moins un MSD. De là grâce à la commande str_count on compte le nombre d’occurence de chaque protéine (MSD, NBD, SBP) dans la colonne Domaine Organisation. On recherche des systèmes ABC complets, soit 2 MSD, 2 NBD et 0 ou plus SBP. On filtre les lignes pour ne garder que celles qui correspondent à nos conditions précédentes.
Dans un deuxième temps on fait le lien entre la table Assembly et la table Protein où on sélectionne Chromosome, Gene_ID, Domain_Structure, Identification_Status. Le but est de ne garder que les lignes dont l’identification est “confirmée” , ainsi on filtrera nos lignes sous cette condition.
Ensuite on renomme la colonne “Assembly” en “Assembly_Name" afin de pouvoir merge les deux data frames obtenus précédemment , on ne conservera qu'un exemplaire des colonnes en communs.
Après on va chercher à lier les tables Gene, Chromosome et Strain entre elles puis on supprimera les colonnes qui ont servi à faire le lien des tables et qui ne serviront donc pas pour l’analyse. L'intérêt est de pouvoir récupérer l’espèce et la lignée. On renommera la colonne “Chr_ID” en “chromosome” afin de pouvoir merger correctement.
On récupère la colonne Function dans la table Gène qui nous servira à avoir des informations supplémentaires lors de l’analyse. Ensuite on fera le lien entre les tables Gene, Conserved Domain et Functional Domain qui contiennent des informations essentielles pour l’analyse tel que le score, la e value ou la description.
Pour finir on merge l’ensemble des data frames.
﻿
La matrice finale est de la forme:
﻿
﻿Gene_ID		 Function	 Chromosome Strain_ID  	  Species	 Assembly_Name	 Domain_Structure	 Identification_Status	Domain_Organization	 Functional_Classification Score	 e_Value Description

Pour la partie analyse de notre projet, nous envisageons de nous servir du score et de la E value pour déterminer pour chaque gène pas encore annoté, si le score et la e-value associée se rapproche de ceux d’un gène annoté (donc une protéine d’un système annoté). Les colonnes chromosomes, espèces, lignée nous permettent d’orienter la recherche vers certaines lignes de la matrice. En effet, il est totalement possible que les gènes nouvellement à classifier aient des espèces, lignées, chromosomes en commun avec les gènes de la matrice.
Une fois une protéine associée à un gène non encore annoté ,on va regarder si on arrive à reformer un système complet et donc trouver des nouveaux systèmes ABC. Les colonnes Function, description, functional classification permettront d’apporter des informations supplémentaires à chaque nouvelles protéines( famille, function…etc).
