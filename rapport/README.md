Pour la remise du projet, un fichier Markdown consitutant le rapport devrait remplacer ce README.

Bien qu'au format markdown, il est attendu un vrai rapport de projet.

# Contexte

Une brêve introduction sur le contexte (ABC) et les objectifs clairs du projet : quelle question est posée.

![Transporteur ABC](transporteurs.png)

# Analyse

Donnez des précisions sur les objectifs à atteindre et comment y arriver. 

Effectuez une ou des analyses préliminaires des données pour les appréhender ainsi que les méthodes disponibles pour atteindre les objectifs.

# Conception

Fort de l'analyse précedente, présenter l'approche choisie et pourquoi. 

Ensuite, présenter quelle méthodologie allez-vous mettre en oeuvre pour les différentes étapes du projet avec quelles méthodes :

  * Tout d'abord, présenter comment va être obtenue la matrice individus-variables+classes (sélection, transformation, nettoyage, ...)
  * Puis, comment elle être utilisée pour produire des résultats (par exemple : quelle(s) méthode(s) de classification ou de clustering)
  * Enfin, quelle méthodologie sera mise en oeuvre pour évaluation les résultats obtenus (quelles mesures ou indicateurs, . 

# Réalisation

Mise en oeuvre de ce qui a été conçu : il s'agit là de préciser les paramètres et tous les détails concernant la réalisation concrète de l'analyse. 

Inclure ensuite les résultats obtenus et/ou leur synthèse s'ils sont trop nombreux pour le format d'un rapport de projet.

# Discussion

Analyse et discussion sur les résultats obtenus. 

Conclusions sur la qualité de la ou des méhtodes mises en oeuvre.

# Bilan et perspectives

Qu'est-ce qui fonctionne ou pas. 

Pistes d'amélioration. 

Recul sur l'ensemble du projet. 

Si c'était à refaire... ou bien, quelles pourraient être les pistes d'investigation subséquentes ouvertes par les résultats obtenus ?

# Gestion du projet

  * Comment s’est organisé le groupe. 
  * Comment se sont déroulées les discussions, les prises de décisions. 
  * Comment se sont répartities les tâches. Qui a fait quoi. 
  * Quels ont été les rôles et les contributions de chacun·e. 
  * Diagramme de Gantt avec le calendrier et les tâches.

