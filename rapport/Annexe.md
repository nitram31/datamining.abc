<h1>Knime</h1>

Bayésien et arbre:
![image](rapport/baeysienarbre.PNG )

Bayésien x partitioner:
![image](rapport/Naivebaeysien.PNG)

Abre de decision x partitioner:
![image](rapport/decisiontree_xaggregator.PNG)

K-means:
![image](rapport/Kmeans.PNG)

PMML:
![image](rapport/pipeline.PNG)


<h1>Résultat Knime</h1>

Decision_tree_relative_30%,_draw_randomly,_gini index;_no_prunning:
![image](rapport/Decision_tree_relative_30,_draw_randomly,_gini_index_no_prunning.png)

Decision_tree_relative_30%,stratified,_gain_ratio,_MDL:
![image](rapport/Decision_tree_relative_30,stratified,_gain_ratio,_MDL.png)

Decision_tree_relative_30%,stratified,_gain_ratio,_no_prunning:
![image](rapport/Decision_tree_relative_30,stratified,_gain_ratio,_no_prunning.png)

Decision_tree_relative_30%,stratified,_gini_index;_no_prunning:
![image](rapport/Decision_tree_relative_30,stratified,_gini_index;_no_prunning.png)

Decision_tree_relative_30%,stratified,gini_index,_MDL:
![image](rapport/Decision_tree_relative_30,stratified,gini_index,_MDL.png)

Decision_tree PMML__MDL,_stratified,__gain_ratio:
![image](rapport/PMML__MDL,_stratified,__gain_ratio.png)

Decision_tree PMML__MDL,_stratified,__gini_index/
![image](rapport/PMML__MDL,_stratified,__gini_index.png)

Decision_tree PMML__no_pruning,_draw_randomly,__gini_index:
![image](rapport/PMML__no_pruning,_draw_randomly,__gini_index.png)

Decision_tree PMML__no_pruning,_stratified,__gini_index:
![image](rapport/PMML__no_pruning,_stratified,__gini_index.png)

Bayésien_draw_randomly:
![image](rapport/Bayesien_draw_randomly.png)

Bayésien_stratified:
![image](rapport/Bayesien_stratified.png)

Bayesien_stratified_x_partitioner:
![image](rapport/Bayesien_stratified_x_partitioner.png)

PMML_bayesien__random:
![image](rapport/PMML_bayesien__random.png)

PMML_bayesien__stratified:
![image](rapport/PMML_bayesien__stratified.png)

PMML_bayesien_stratified_x_partitioner:
![image](rapport/PMML_bayesien_stratified.png)

K-means:
![image](rapport/Kmeansresutlat.PNG)
