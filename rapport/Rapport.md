Coraline SIPRA
Martin RACOUPEAU
Master 1 BBS 2021-2022

Compte-rendu de projet Fouille de données



<h1>Contexte:</h1>

Les transporteurs ABC regroupent un grand ensemble de protéines transmembranaires dont le rôle est le transport (import, export) de part et d'autre de la membrane cytoplasmique de diverses substances. Ils sont impliqués dans divers rôles, notamment la résistance aux antibiotiques. 

![image](rapport/transporteurs.png)

Ils sont composés de protéines fonctionnant en dimère: MSD (domaine transmembranaire), NBD (domaine de liaison au nucléotide) et dans le cas des importeurs un SBP (protéine de fixation des ligands solubles). Ils forment un système où chaque domaine/protéine peut être codé par des gènes différents ou bien tout le système peut être codé par un gène.
<h4>Objectif:</h4>

Ce projet à pour but l’annotation d’un nouveau génome à partir des données expertisées.

<h1> Analyse:  </h1>


Le but est de pouvoir prédire si un génome contenu dans la partie non expertisée de la base de données peut être prédit en tant que ABC ou non. Pour ce faire nous nous sommes servi comme référence des génomes qui ont déjà été prédit comme ABC dans la base données (données expertisées). 
Nous avons utilisé les identifiants des gènes afin de faire le lien entre les données. De plus, nous nous sommes servis de la table Orthology. Une orthologie est un lien évolutif entre deux gènes présents chez deux espèces différentes, c’est donc une indication d’un lien entre deux gènes qui pourraient être potentiellement ABC.

Les analyses préliminaires réalisées étaient une première prise de contact avec les données. Certaines tables étant très importantes, nous avons donc observé la structure et le contenu des tables en limitant les affichages à 100 lignes. 

Nous avons aussi réalisé des comptages lorsque certaines entités des tables de la base étaient regroupées en plusieurs catégories, afin de voir la répartition des effectifs dans chaque catégorie.


Plusieurs méthodes ont été utilisées pour réaliser ce projet. Premièrement l’utilisation de Mariadb (système de gestion de base de données) afin de charger les tables de la base. Aussi l’utilisation de Rstudio avec les packages Rmysql de façon à parcourir et générer des dataframes. L’utilisation du package dplyr dans le but de filtrer et regrouper nos données. De plus nous nous sommes servi de R et de Python de façon à générer les matrices. Pour finir, Knime à servi à réaliser la partie analyse de nos matrices.

<h1> Conception: </h1>


<h2>Réalisation de la matrice:</h2>

Tout d’abord, afin de créer les jeux de données pour knime, nous allons générer deux matrices de comptage : une première qui contiendra le jeu d'entraînement et de test, c'est-à-dire les gènes qui sont sûrs d'être ou ne pas être ABC. La deuxième contiendra les gènes contenus dans la partie non expertisée de la base de données et que nous allons essayer de prédire en tant  qu'ABC ou non.

<h4>Trouver les gènes qui sont sûrs d'être ABC:</h4>
Tout d’abord nous avons sélectionné avec des requêtes SQL l’ensemble des entités contenues dans la table Assembly (table qui contient les systèmes ABC reconstruits). Seulement les lignes (système) qui contiennent la protéine MSD ont été sélectionnées (car le MSD est une protéine toujours présente dans les systèmes complets, si il n’y en a pas, on considère que ce n’en n’est pas un ). Ensuite parmis tous ces système obtenu nous avons sélectionné les lignes dans “Domain Organization” qui ne contenaient que des systèmes complets (avec au moins 2 MSD, 2 NBD) et ce grâce la fonction count de R pour compter le nombre de protéine et filtrer ensuite le résultat.
Ensuite nous avons récupéré les Assembly, Chromosome, Gene_ID, Domain_Structure et Identification_Statut depuis la table Protein et nous avons filtré pour ne garder que ceux qui avaient un statut d’identification confirmé. Enfin nous avons fusionné les deux dataframe généré pour n’avoir que des gènes issu de système ABC complet et confirmé.

<h4>Ajout d’information issu de d’autres tables:</h4>
Dans la première version de ce projet, nous voulions nous servir de différentes informations contenues dans les tables Strain, Chromosome,Species,Conserved Domain, Functional Domain tel que le score, la e_value, la lignée ou encore l’espèce afin de pouvoir discriminer les Gene_ID et déterminer ceux potentiellement ABC dans les non annoté. Cette idée à ensuite été abandonnée pour se concentrer sur l’orthologie. Cette matrice ayant quand même servi de base pour la matrice de comptage, elle contient donc à cette étape des reliquats de nos essais précédents, tels que des colonnes issues des tables Strain, Species, Chromosome, Functional Domain ou encore Conserved Domain. L’ensemble des requêtes ont été fusionné dans le but de générer un grand data frame contenant les gènes étant sûrs d'être ABC.

Nous nous retrouvions avec les Gene_ID en format scientifique , nous avons donc chaque fois converti les Gene_ID dans le bon format à l’aide d’un sapply().

<h4>Récupérer les informations de orthologie:</h4>
Comme nous l’avions expliqué dans la partie précédente , notre annotation va se servir de la table orthologie afin de pouvoir analyser les liens entre la protéine et les sous-familles des protéines qui lui sont liées. Nous avons donc récupéré les Query_ID, Subject_ID, identity et alignment length d'orthology que nous avons fusionné avec les Gene_ID issus de Protein pour former une grande matrice.

Nous avons sélectionné identity, alignement length afin de voir si nous pouvions filtrer cette matrice qui contenait plus de 8 millions de lignes, afin de réduire le temps de traitement. 
Après avoir réalisé des filtres, nous avons abandonné et conservé la matrice originelle. En effet nous perdions des informations importantes pour par la suite reconnaître les Gene_id.


<h4>Générer la matrice de comptage du jeu d'entraînement :</h4>

<h4>Gènes sûrs d'être ABC:</h4>


![image](rapport/df_ortho.PNG)

Nous avons récupéré le data frame obtenu en tout début et qui contient les gènes sûrs d'être ABC. A ce niveau là toutes les autres colonnes de notre matrice à part le subfamily sont retirées. La colonne subfamily (sous famille ABC) est essentielle pour notre analyse. 

Dans un premier temps nous avons généré une nouvelle matrice qui ne conserve que les Gene_ID et avons créée des colonnes correspondant à chaque sous famille ABC possible (de A1 à A12), toutes les cases correspondantes aux sous familles ont été mise à 0 car il s’agit d’une matrice de comptage.
En parallèle nous avons généré une “matrice temporaire” qui ne contient cette fois ci que le subfamily associé à chaque Gene_id . Cette matrice ne nous servira que pour le script que nous allons utiliser.

Dans un deuxième temps nous avons rempli notre matrice de comptage. Le but était de savoir à quelles sous familles les Gene_id avaient un lien orthologique. Ce script parcourt donc les Gene_Id contenu dans orthologie et récupère le gène "Subject id” avec lequel il est en orthologie. Ce subject id est mis en relation avec le Gene_ID issu de la matrice temporaire. Si un même Gène_ID est trouvé on regarde la sous-famille associée. La matrice de comptage est alors incrémentée de 1 pour la case de la sous-famille qui correspond à ce gène ID.

Le temps de génération des matrices de comptage a été particulièrement long, notamment pour celle-ci. Cela a duré plus de 80 heures au total, notre script avait une complexité trop élevée par rapport au nombre de lignes à traiter.

Certains Gene_ID étant restés à 0 sur toutes les cases, ils ont été retirées de la matrice, afin de ne pas fausser le jeu de test.

<h4>Gènes sûrs de ne pas être ABC:</h4>

![image](rapport/not_abc.PNG)

Pour ce faire nous sommes allés dans la table Protein et nous avons filtré les Gene_ID pour ne conserver que les gènes qui avaient leur colonne “Type” différente de ABC. Ces gènes ont été récupérés et sur le même principe qu’avant ont été mis sous la forme d’une matrice de comptage, mais cette fois-si toutes les cases seront à 0 (en effet s'ils ne sont pas ABC alors il n’y a pas de sous-famille).

Ces gènes ont ensuite été ajoutés à la matrice de comptage des gènes ABC afin de générer le jeu d'entraînement.
Une colonne class a été ajoutée pour permettre l’analyse sur Knime. Il pouvait arriver que dans certains cas il y ait plusieurs sous famille pour un gène, cela à donc été filtré pour que la colonne class ne contiennent que la sous famille majoritaire (Knime pouvait quand même décider par lui même car toutes les valeurs des cases on été conservé) ou OTHER si il ne s’agit pas d’un ABC. 

<h4>Générer la matrice à annoter:</h4>

![image](rapport/not_annotated.PNG)

Depuis la table Gene nous avons sélectionné les Gene_ID qui n'étaient pas contenus dans la table Protein (la partie expertisée de la base). Comme sur le principe précédent, on a généré une matrice de comptage où toutes les cases étaient mises à 0.
Vu que Knime a besoin de colonnes identiques, on a aussi généré une colonne class où toutes les lignes ont été mise à “Potentiel ABC”


<h2>Knime:</h2>

Différentes approches seront testées afin de déterminer la méthode la plus intéressante pour annoter nos gènes. A chaque fois nous testerons différents paramètres dans le but d’affiner les prédictions (les images des pipelines sont en annexes).

<h4>Méthode</h4>

***Arbre de décision:****
Cette méthode se prête bien à nos données étant donné que nous avons un jeu de données de classes connues. De plus, notre jeu été pré-traité ce qui pallie au problème de données manquantes et de bruit. Différents paramètres seront testés.
Une version avec un PMML afin d'entraîner notre jeu de données a été testé. C’est un langage de balisage conçu pour permettre la description des modèles d’analyse prédictive qui peuvent être partagées entre les différents systèmes et applications. 
![image](rapport/decisiontree_xaggregator.PNG)


***Naive bayesian:***
C’est un modèle prédictif qui prédit plusieurs hypothèses, pondérées par leur probabilité, ce qui peut être intéressant vu qur l’on a plusieurs sous-famille pour un gène, de plus, nos classes sont liées. Là aussi une version avec PMML sera testé.
![image](rapport/Naivebaeysien.PNG)

***Clustering K-means:***
Nous avons plusieurs sous-familles et nous recherchons de la similarité entre les gènes, on peut donc essayer de réaliser un K-means. Le k-medoids n'était pas intéressant car nos données sont contrôlées, il n'y a donc pas trop de problème de données aberrantes.
![image](rapport/Kmeans.PNG)


<h4>Analyse des résultats:</h4>

Notre but est de déterminer la méthode et les paramètres les plus intéressants afin d’annoter nos gènes. Pour ce qui est de l’arbre de décision et du naive bayesian nous allons observer les scores obtenus dans la matrice de confusion (scorer) à chaque fois et sélectionner celle où les résultats sont les meilleurs principalement pour la sensibilité (capacité de la méthode à évaluer tout ce que l’on cherche), la spécificité (capacité à bien prédire) mais aussi les autres mesures telles que Accuracy, recall..etc. Pour ce qui est du clustering , nous regarderons la prédiction faite ainsi que le coefficient de silhouette (mesure de qualité d'une partition d'un ensemble de données).

Une fois la méthode sélectionnée,nous récupérerons le modèle généré (PMML) et l’appliquerons sur nos données à annoter. De là, en fonction de la prédiction, nous nous assurerons que ces gènes soient bien retrouvés dans la base de données originelle.

<h1> Réalisation: </h1>

***Arbre de décision:***
Différents paramètres sont testés :

- partitionnement de nos données au hasard ou stratifié (car nous avons une répartition des classes hétérogènes, afin de compenser)

- le coefficient de Gini (mesure la dispersion d’une distribution dans une population)

- le gain ratio (utilisé pour normaliser le gain d'information d'un attribut par rapport à la quantité d'entropie de cet attribut.)

- l’élagage (identification et suppression des branches correspondant à des exceptions ou du bruit ou non

- MDL (Minimum Description Length principe pour élaguer)


Nous avons aussi testé la cross validation car certaines sous-familles sont sous représentées.

***Naive Bayesian:***
Un peu sur le même principe qu’avant, nous avons testé l'influence du partitionnement (randomly, stratified) et de la cross validation.

***Clustering:***
Nous avons testé avec un partitionnement (randomly, stratified) mais nous ne sommes pas allés plus loin vu que les résultats étaient peu satisfaisant (voir plus bas).

<h4>Résultat:</h4>

![image](rapport/tableau.PNG)


Chaque méthode a été testé avec différents paramètres et nous avons observé principalement l’évolution de la spécificité, sensibilité et accuracy. Pour les 5 méthodes testées, nous avons sélectionné celle avec les meilleurs resultats. Une fois sélectionnée, nous avons réalisé une sélection finale entre les 4 méthodes et paramètres sélectionnés afin de n’en conserver qu’une seule qui sera appliquée à notre jeu de données a prédire.

Le clustering a vite été abandonné, en observant les résultats obtenus , il s’avère que le coefficient de silhouette était négatif (mauvaise classification) pour la plupart des prédictions. 

La cross-validation (x-partitionner) s’est trouvée être une méthode intéressante pour inclure certaines de nos sous-famille sous représentées (A9 et A12). La stratification été aussi une option intéressante car cela a permis de prendre en compte le déséquilibre d’effectif de certaines sous-famille.

Pour ce qui est des arbres de décision, l'indice de Gini favorise les grandes partitions (distributions) alors que le gain d'information prend en charge les petites partitions (distributions) avec différentes valeurs distinctes. Ce qui explique pourquoi le gain ratio est notre meilleur paramètre car nous avons des sous-famille très peu représentées.

Le PMML (Predictive Model Markup Language) s’avère être un bon moyen d’améliorer nos résultats. En effet, entraîner nos arbres et importer le modèle obtenu(en ayant préférablement choisi les bons paramètres) permet d’améliorer les performances de prédiction et de classification.

<h5>Les 4 méthode et paramètres conservées après analyse:</h5>


Arbre de décision: Gain ratio, no pruning, x partitionner, stratified:

![image](rapport/Gain_ratio,_no_prunning;_x_aggregator.png)

PMML Arbre de décision: x-partitionner, stratified, gain ratio, MDL:

![image](rapport/PMML_decision_tree_x-aggregator,_stratified,_gain_ratio,_MDL.png)

Bayésien: stratified, x partitioner:

![image](rapport/Bayesien_stratified_x_partitioner.png)

PMML Bayésien: stratified:

![image](rapport/PMML_bayesien__stratified.png)


Après une dernière comparaison entre ces 4 méthodes, au vu des résultats, nous avons conservé l’arbre de décision: Gain ratio, no pruning, X-partitionner pour générer le modèle PMML et l’appliquer sur nos jeux de données.

<h4>Résultat de PMML predictor:</h4>

![image](rapport/Capture.PNG)

Nous avons trouvé 20 observation potentiellement ABC, toutes étaient bien présentes dans la base de données. Les gènes se trouvaient dans différentes familles. En regardant la lignée nous avons réussi à annoter 5 génomes en ABC.


Nous avons donc identifié 5 génomes qui pourraient être étudiés plus précisément pour savoir si les gènes présents font partie d’un système ABC complet.

<h1> Discussion </h1>


Après avoir généré une matrice de comptage contenant les gènes qui sont sûrs de ne pas être ou être ABC, nous avons déterminé que la meilleure méthode pour prédire de nouveau système ABC était d'utiliser sur Knime un Arbre de décision ayant comme paramètre un X partitionner, Gain ratio et no pruning. Nous avons généré un modèle PMML que nous avons appliqué à notre matrice de gène à prédire. Nous avons présenté 319 623 gènes et avons obtenu 20 gènes potentiellement ABC.

Le fait que nous ayons obtenu seulement 20 gènes peut s’expliquer par le fait que notre matrice de test ne contenait que des gènes issus de système complet, ce qui réduit le champ de possibilité pour analyser.

La méthode PMML a généré de bons résultats, cependant du fait que les sous-famille A9 et A12 soient sous représentées induit qu’elle ne sont pas aussi bien prédite que les autres malgré la mise en place d’une cross validation et de stratification.

<h1> Bilan et perspectives </h1>

Ce projet comporte certaines pistes d’amélioration, notamment le fait d’améliorer les scripts dans le but de limiter la complexité et ainsi réduire le temps de calcul nécessaire pour parcourir orthologie.

Nous nous sommes basés pour la réalisation de la matrice de comptage sur des gènes issus de systèmes complets. Une chose qui aurait pu être envisagée aurait été de baser sur l’ensemble des système prédit dans la partie expertisée afin d’avoir un plus grand choix dans la matrice de comptage et ainsi peut être prédire plus de génome.

Maintenant que nous avons obtenu 5 génomes, il serait intéressant de déterminer à quelle protéine du système ABC correspondent les gènes en question.

<h1> Gestion du projet </h1>

Le travail a été commun dans la grande partie du projet, certaines analyses ne fonctionnant que sur un ordinateur sur les deux (notamment la partie orthology), elles ont été réalisées puis partagées afin d’être contrôlées en groupe. 

Les prises de décision se sont réalisées d’un commun accord après discussion et la répartition des tâches a été uniforme dans la majorité (Coraline ayant rédigé un peu plus le projet pendant que Martin a travaillé un peu plus sur la portion rstudio), on peut donc affirmer une répartition 50/50 sur l’ensemble du projet.

***Diagramme de Gantt:***

![image](rapport/gantt.png)

